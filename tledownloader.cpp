#include "tledownloader.h"
#include "ui_tledownloader.h"
#include <iomanip>

TLEDownloader::TLEDownloader(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::TLEDownloader)
{
    ui->setupUi(this);

    db = QSqlDatabase::addDatabase("QMYSQL");
    db.setHostName("localhost");
    db.setPort(3306);
     this->processType = 3; // set process 1 one table/2 seperate table/3 day by day table
    qDebug() << "test2222222222222222222";
//    if(this->processType == 1) {db.setDatabaseName("space_object"); qDebug() << "1 table";}
//    else if(this->processType == 2){ db.setDatabaseName("separate_space_object"); qDebug() << "multiple table";}
    db.setDatabaseName("dbd_space_object"); qDebug() << "day by day table";

    db.setUserName("root");
    db.setPassword("root");
    bool ok = db.open();
    qDebug() << ok;

//     this->getTLEData(0); //full = 0; LEO = 1
//    this->readTLETextFile("C:/Users/Admin/Desktop/tleDownloader/test_tle.txt");
    this->send_mail();

//      QTimer *timer = new QTimer(this);
//      connect(timer, SIGNAL(timeout()), this, SLOT(test_timer()));
//      timer->start(3600000);
}



void TLEDownloader::replyFinished(QNetworkReply *reply)
{
    QTime myTimer;
    int statusCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    reply->attribute(QNetworkRequest::HttpStatusCodeAttribute);
    QString statusName = reply->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toString();
    qDebug() << "Status code " << statusCode << statusName;

    QByteArray bytes = reply->readAll();
    QString info = QString::fromUtf8(bytes.data(), bytes.size());

    if(statusCode == 200)
    {
        QStringList text = info.split("\r\n", QString::SkipEmptyParts);
        int numTLEdownload = text.size()/2;
        this->compareAmountTLE(numTLEdownload);

        if(text.size() > 0){
            this->saveTLEtxt(bytes);
            this->clear_updating();
            // set start time
            QDateTime start =  QDateTime::currentDateTimeUtc();
            qDebug() << "start" << start;

            //loop insert TLE to DB
            myTimer.start();
            for(int i = 0; i < text.size(); i++){
                //get line1 and line2 and send to getTLEData function
                this->receiveTLEData(text[i]);
//                if(fo) stream << text[i] << '\n';
            }

            // set end time
            QDateTime end =  QDateTime::currentDateTimeUtc();
            qDebug() << "end" << end;
            qDebug() << "use time :" << QString("%1").arg(myTimer.elapsed()/1000.0) << " sec";
            this->save_time_process(start,end,numTLEdownload); // save time


        }else{
            qDebug() << "TLE data is 0 object please check space-track query link";
            qDebug() << "URL : " << API_ST_URL;
        }
    }
    else
    {
        qDebug() << reply->error() << reply->errorString();
    }

    reply->deleteLater();
}

void TLEDownloader::getTLEData(int tleType)
{
    this->tleType = tleType;

    QUrl url("https://www.space-track.org/ajaxauth/login");
    QNetworkRequest request(url);

    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");


    QUrlQuery urlQuery;
    urlQuery.addQueryItem("identity", "osa@gistda.or.th");
    urlQuery.addQueryItem("password", "Gistda1234567890");
    urlQuery.addQueryItem("query", API_ST_URL);
    QString params = urlQuery.query();
//    qDebug() << "param :" << params;

    manager = new QNetworkAccessManager(this);
    connect(manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(replyFinished(QNetworkReply*)));

//    qDebug() << "method POST";
    manager->post(request, params.toUtf8());

}

int TLEDownloader::receiveTLEData(QString tleData)
{
    int successChecker;
    if (tleData.mid(0,1) == "1"){
        //split line1
        line1 = tleData;
        this->results << tleData.mid(2,5).trimmed(); //satellite number
        this->results << tleData.mid(7,1).trimmed(); //classification
        this->results << tleData.mid(9,2).trimmed(); //launch year
        this->results << tleData.mid(11,3).trimmed(); //launch number of the year
        this->results << tleData.mid(14,3).trimmed(); //piece of the luanch
        this->results << tleData.mid(18,2).trimmed(); //epoch year
        this->results << tleData.mid(20,12).trimmed(); //epoch day of year
        this->results << tleData.mid(33,10).trimmed(); //mean motion devided by two
        this->results << tleData.mid(44,8).trimmed(); //mean motion devided by six
        this->results << tleData.mid(53,8).trimmed(); //BSTAR
        this->results << tleData.mid(62,1).trimmed(); //ephemeris type
        this->results << tleData.mid(64,4).trimmed(); //element set number
        this->results << tleData.mid(68,1).trimmed(); //checksum
    }else if (tleData.mid(0,1) == "2"){
        //split line2
        line2 = tleData;
        this->results << tleData.mid(2,5).trimmed();
        this->results << tleData.mid(8,8).trimmed();
        this->results << tleData.mid(17,8).trimmed();
        this->results << tleData.mid(26,7).trimmed();
        this->results << tleData.mid(34,8).trimmed();
        this->results << tleData.mid(43,8).trimmed();
        this->results << tleData.mid(52,11).trimmed();
        this->results << tleData.mid(63,5).trimmed();
        this->results << tleData.mid(68,1).trimmed();
        if(this->checkTLEFormat(line1,line2) && this->checksumTLE(line1) && this->checksumTLE(line2)){
            successChecker = this->insertTLE(results);
            this->filterSuccessChecker(successChecker);
        }else{
            qDebug() << "TLE Format ERROR ON";
            qDebug() << line1;
            qDebug() << line2;
            nfor++;
        }
        this->results.clear();

//        qDebug() << "insert transaction success :" << successChecker;

    }else{
        this->results.clear();
    }
    return successChecker;
}

int TLEDownloader::insertTLE(QStringList tleData)
{
    int result;
    QString strQuery = "";
    //qDebug() << "start insert" << QDateTime::currentDateTimeUtc().toString("dd/MM/yyyy HH:mm:ss.zzz");
    strQuery += "SET @psat_no = " + tleData[0] + ";";
    strQuery += "SET @pclassification = '" + tleData[1] + "';";
    if(tleData[2] != ""){
        strQuery += "SET @plaunch_year = '" + tleData[2] + "';";
    }else{
        if(this->processType == 2) strQuery += "SET @plaunch_year = '-';"; //use for separate_space_object table
        else strQuery += "SET @plaunch_year = NULL;"; //use for space_object table

    }

    if(tleData[3] != ""){
        strQuery += "SET @plaunch_no = '" + tleData[3] + "';";
    }else{
        if(this->processType == 2) strQuery += "SET @plaunch_no = '-';"; //use for separate_space_object table
        else strQuery += "SET @plaunch_no = NULL;"; //use for space_object table
    }

    if(tleData[4] != ""){
        strQuery += "SET @ppiece_launch = '" + tleData[4] + "';";
    }else{
        if(this->processType == 2) strQuery += "SET @ppiece_launch = '-';"; //use for separate_space_object table
        else strQuery += "SET @ppiece_launch = NULL;"; //use for space_object table
    }

    strQuery += "SET @pepoch_year = '" + tleData[5] + "';";
    strQuery += "SET @pepoch_doy = " + tleData[6] + ";";
    strQuery += "SET @p1st_mean_motion = '" + tleData[7] + "';";
    strQuery += "SET @p2nd_mean_motion = '" + tleData[8] + "';";
    strQuery += "SET @pbstar = '" + tleData[9] + "';";
    strQuery += "SET @pephe_type = " + tleData[10] + ";";
    strQuery += "SET @pelement_no = " + tleData[11] + ";";
    strQuery += "SET @pchecksum_line1 = " + tleData[12] + ";";
    strQuery += "SET @pinclination = " + tleData[14] + ";";
    strQuery += "SET @pRAAN = " + tleData[15] + ";";
    strQuery += "SET @peccentricity = '" + tleData[16] + "';";
    strQuery += "SET @parg_of_perigee = " + tleData[17] + ";";
    strQuery += "SET @pmean_anomaly = " + tleData[18] + ";";
    strQuery += "SET @pmean_motion = " + tleData[19] + ";";
    strQuery += "SET @prev_no = " + tleData[20] + ";";
    strQuery += "SET @pchecksum_line2 = " + tleData[21] + ";";
    if(this->processType == 1) strQuery += "CALL insertTLE(@psat_no, @pclassification,@plaunch_year,@plaunch_no,@ppiece_launch,@pepoch_year,@pepoch_doy,";
    else if(this->processType == 2) strQuery += "CALL manage_TLE_data(@psat_no, @pclassification,@plaunch_year,@plaunch_no,@ppiece_launch,@pepoch_year,@pepoch_doy,";
    else if(this->processType == 3) strQuery += "CALL dbdTLEUpdate(@psat_no, @pclassification,@plaunch_year,@plaunch_no,@ppiece_launch,@pepoch_year,@pepoch_doy,";
    strQuery += "@p1st_mean_motion,@p2nd_mean_motion,@pbstar,@pephe_type,@pelement_no,@pchecksum_line1,@pinclination,";
    strQuery += "@pRAAN,@peccentricity,@parg_of_perigee,@pmean_anomaly,@pmean_motion,@prev_no,@pchecksum_line2, @pResult);";

    // pResult = 0 is satellite is not found, pResult = 1 is satellite and epoch are found, pResult = 2 is satellite is found but epoch is not found ;
    QSqlQuery query;
    query.exec(strQuery);
    query.exec("SELECT @pResult AS 'pResult';");
    query.next();
    result = query.value(0).toInt();

    return result;
}

void TLEDownloader::selectDATA()
{
    QSqlQuery query;
    qDebug() << "start" << QDateTime::currentDateTimeUtc().toString("dd/MM/yyyy HH:mm:ss.zzz");
    query.exec("SELECT * from 33396u order by id desc");
    while(query.next()){
        qDebug() << query.value(1).toString() << query.value(2).toString() << query.value(3).toString() << query.value(4).toString()
                    << query.value(5).toString() << query.value(6).toString() << query.value(7).toString() << query.value(8).toString()
                       << query.value(9).toString() << query.value(10).toString() << query.value(11).toString() << query.value(12).toString()
                          << query.value(13).toString() << query.value(14).toString() << query.value(15).toString() << query.value(16).toString();
        qDebug() << "end" << QDateTime::currentDateTimeUtc().toString("dd/MM/yyyy HH:mm:ss.zzz");
    }
}

void TLEDownloader::save_time_process(QDateTime start,QDateTime end,int adl_tle)
{
    QSqlQuery query;

    query.prepare("INSERT INTO report (amount_tle, num_repeat_tle, start, end, num_not_updated ,num_updated , num_new_tle, num_error, num_format_error, amount_tle_after_update, num_5d_no_update) "
                  "VALUES (?, (SELECT count(id) FROM dbd_space_object.tle_data where updating > 1), ?, ?, ?, ?, ?, ?, ?, (SELECT count(id) FROM dbd_space_object.tle_data), (SELECT count(id) FROM dbd_space_object.tle_data where updating = 0))");
    query.addBindValue(adl_tle);
    query.addBindValue(start);
    query.addBindValue(end);
    query.addBindValue(n1);
    query.addBindValue(n2);
    query.addBindValue(n3);
    query.addBindValue(ner);
    query.addBindValue(nfor);
    query.exec();
//    qDebug() << query.lastError().text();
}

void TLEDownloader::clear_updating()
{
    QSqlQuery query;
    n1 = 0; n2 = 0; n3 = 0; ner = 0 ; nfor = 0;

    query.prepare("UPDATE tle_data SET updating=0");
    query.exec();
//    qDebug() << query.lastError().text();
}


void TLEDownloader::test_timer()
{
    this->getTLEData(0);
}

void TLEDownloader::filterSuccessChecker(int s)
{
    switch(s) {
       case 1  :
          n1++;
          break; //not update
       case 2  :
          n2++;
          break; //update
       case 3  :
          n3++;
          break; //insert
       default :
          ner++;//error
    }
}

void TLEDownloader::compareAmountTLE(double tle_num)
{
    QSqlQuery query;
    double temp = 0;
    query.exec("SELECT amount_tle FROM report ORDER BY id DESC LIMIT 1;");
    if (query.next()) {
          temp = query.value("amount_tle").toInt();
    }
    qDebug() << "-------------------------------------";
    qDebug() << "amount of TLE data downloaded: ";
    qDebug() << temp << " >>>>> " << tle_num;
    double percentChange = ((tle_num/temp)*100.0)-100.0;
    qDebug() <<  percentChange << "%";
    if(qAbs(percentChange) > 0.1) qDebug() << "Data is Warning!";
    else qDebug() << "Data is Good!";
    qDebug() << "-------------------------------------";
}

bool TLEDownloader::readTLETextFile(QString filePath)
{
    QFile inputFile(filePath);
    if (inputFile.open(QIODevice::ReadOnly))
    {
       QTextStream in(&inputFile);
       QString all = in.readAll();
       QStringList text = all.split("\r\n", QString::SkipEmptyParts);

       this->clear_updating();
       int numTLEdownload = text.size()/2;
       this->compareAmountTLE(numTLEdownload);

       QDateTime start =  QDateTime::currentDateTimeUtc();
       qDebug() << "start" << start;
       for(int i = 0; i < text.size(); i++){
           this->receiveTLEData(text[i]);
       }
       QDateTime end =  QDateTime::currentDateTimeUtc();
       qDebug() << "end" << end;

       this->save_time_process(start,end,numTLEdownload); // save time

       inputFile.close();
       return true;
    }else{
        qDebug() << "fail";
        return false;
    }
}

bool TLEDownloader::checksumTLE(QString line)
{
    QStringList L = line.split("", QString::SkipEmptyParts);
    int cksum = 0;
    for(int i=0; i < L.size() - 1; i++){
        QString c = L[i];
        if(c == " " || c == "." || c == "+" || c.at(0).isLetter())
            continue;
        else if(c == "-")
            cksum = cksum + 1;
        else
            cksum = cksum + c.toInt();
    }

    if(cksum%10==line.at(68).digitValue())
        return true;
    else
        return false;

}

bool TLEDownloader::checkTLEFormat(QString line1, QString line2)
{
    QString regexTextLine1 = "^1\\s{1,5}\\d{1,5}[A-za-z]\\s{1,9}[\\d{1,5}\\s{0,3}[A-za-z]*]*\\s{1,3}\\d{5}.\\d{8}\\s{1,2}[+-]*.\\d{8}\\s{1,2}[+-]*\\d{5}[+-]\\d{1}\\s{1,2}[+-]*\\d{5}[+-]\\d{1}\\s\\d{1}\\s{1,4}\\d{1,5}";
    QString regexTextLine2 = "^2\\s{1,5}\\d{1,5}\\s{1,3}\\d{1,3}.\\d{1,4}\\s{1,3}\\d{1,3}.\\d{1,4}\\s\\d{7}\\s{1,3}\\d{1,3}.\\d{1,4}\\s{1,3}\\d{1,3}.\\d{1,4}\\s{1,2}\\d{1,2}.\\d{8}\\s{0,4}\\d{1,5}\\d{1}";

    QRegularExpression re(regexTextLine1);
    QRegularExpressionMatch match = re.match(line1);
    if(!match.hasMatch()){
        qDebug() << "line1 not match";
        return false;
    }

    QRegularExpression re2(regexTextLine2);
    match = re2.match(line2);
    if(!match.hasMatch()){
        qDebug() << "line2 not match";
        return false;
    }

    return true;
}

void TLEDownloader::saveTLEtxt(QByteArray bytes)
{
    QString filename="D:/tleDownloader/test_tle.txt";
    if (QFile::exists(filename))
        QFile::remove(filename);
    QFile file( filename );
    if (file.open(QIODevice::WriteOnly)) {
        file.write(bytes);
        file.flush();
        file.close();
    }else{
        qDebug() << "save txt file error!!";
    }

}

void TLEDownloader::send_mail()
{
    QString user = "osa@gistda.or.th";
    QString pass = "gistda1234";
    QString to = "phasawee@gistda.or.th";
//    QStringList to = { "phasawee@gistda.or.th", "mys.pp@hotmail.com", "phasawee@gmail.com" };
    QString subject = "TLE downloaded report.";
    QString text = "Test";

    Smtp* smtp = new Smtp(user, pass, "smtp.gmail.com", 465);
    connect(smtp, SIGNAL(status(QString)), this, SLOT(mailSent(QString)));

    smtp->sendMail(user, to , subject,text);
}

void TLEDownloader::mailSent(QString status)
{
    if(status == "Message sent")
        qDebug() << "Email sent";
}

TLEDownloader::~TLEDownloader()
{
    db.close();
    delete ui;
}

#ifndef TLEDOWNLOADER_H
#define TLEDOWNLOADER_H

#include <QMainWindow>
#include <QNetworkAccessManager>
#include <QProcess>
#include <QtDebug>
#include <QUrlQuery>
#include <QNetworkReply>
#include <QFile>
#include <QSqlQuery>
#include <QSqlError>
#include <QDate>
#include <QTimer>
#include <QRegularExpression>
#include <QAbstractSocket>
#include "smtp.h"
//#include "src/SmtpMime"

namespace Ui {
class TLEDownloader;
}

class TLEDownloader : public QMainWindow
{
    Q_OBJECT

public:
    explicit TLEDownloader(QWidget *parent = 0);
    ~TLEDownloader();

public slots:
    void replyFinished(QNetworkReply *reply);
    void getTLEData(int tleType); //0 = 2line full, 1 = 2line LEO
    int receiveTLEData(QString tleData);
    int insertTLE(QStringList tleData);
    void selectDATA();
    void save_time_process(QDateTime start,QDateTime end,int adl_tle);
    void test_timer();
    void filterSuccessChecker(int s);
    void clear_updating();
    void compareAmountTLE(double tle_num);
    bool readTLETextFile(QString filePath);
    bool checksumTLE(QString line);
    bool checkTLEFormat(QString line1, QString line2);
    void saveTLEtxt(QByteArray bytes);
    void send_mail();
    void mailSent(QString status);

private:
    QString API_ST_URL = "https://www.space-track.org/basicspacedata/query/class/tle_latest/ORDINAL/1/EPOCH/%3Enow-5/orderby/NORAD_CAT_ID/format/tle";
    int tleType;
    int processType;
    //variable store TLE Data
    QStringList results;
    QString line1;
    QString line2;
    //variable count of num TLE process debug
    int n1;
    int n2;
    int n3;
    int ner;
    int nfor;

    QNetworkAccessManager *manager;
    QProcess *processCDM;

    QSqlDatabase db;
    Ui::TLEDownloader *ui;
};

#endif // TLEDOWNLOADER_H
